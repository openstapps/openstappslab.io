/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Api} from '@openstapps/gitlab-api/lib/api';
import {Group} from '@openstapps/gitlab-api/lib/types';
import {Logger} from '@openstapps/logger';
import {AddLogLevel} from '@openstapps/logger/lib/transformations/add-log-level';
import {Colorize} from '@openstapps/logger/lib/transformations/colorize';
import {GITLAB_API_URL, GROUPS} from '@openstapps/projectmanagement/lib/configuration';
import {Command} from 'commander';
import {readFileSync} from 'fs';
import {resolve} from 'path';
import {generateDocumentation, generatePage, generateReport} from './common';

Logger.setTransformations([
  new AddLogLevel(),
  new Colorize(),
]);

const commander = new Command();

// configure commander
commander
  .version(JSON.parse(readFileSync(resolve(__dirname, '..', 'package.json'))
    .toString()).version)
  .action(async () => {
    // instantiate GitLab API
    const gitlabApi = new Api(GITLAB_API_URL, '');

    // get group info
    const searchResult: Group[] = await gitlabApi.makeGitLabAPIRequest('groups?search=openstapps') as Group[];

    // select correct group
    const group = searchResult.find((searchResultGroup) => searchResultGroup.id === GROUPS[0]);

    if (typeof group === 'undefined') {
      throw new Error(`Group with ID '${GROUPS[0]}' could not be found!`);
    }

    // generate page
    await generatePage(gitlabApi, group);

    // generate report
    await generateReport(gitlabApi, group);

    // generate documentation
    await generateDocumentation(gitlabApi, group);
  })
  .parse(process.argv);
