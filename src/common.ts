/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {asyncPool} from '@krlwlfrt/async-pool/lib/async-pool';
import {Api} from '@openstapps/gitlab-api';
import {Group, Project, TreeFile} from '@openstapps/gitlab-api/lib/types';
import {Logger} from '@openstapps/logger';
import {GROUPS} from '@openstapps/projectmanagement/lib/configuration';
import {getIssuesGroupedByAssignees, getNextMeetingDay} from '@openstapps/projectmanagement/lib/tasks/report';
import {mkdirSync, PathLike, readdirSync, readFile, readFileSync, writeFile} from 'fs';
import got from 'got';
import {marked} from 'marked';
import moment from 'moment';
import mustache from 'mustache';
import {render} from 'mustache';
import {basename, join, resolve} from 'path';
import {promisify} from 'util';

export const readFilePromisified = promisify(readFile);
export const writeFilePromisified = promisify(writeFile);

/**
 * Promisified version of marked
 *
 * @param markdown Markdown to render
 */
export async function markedPromisified(markdown: string): Promise<string> {
  return new Promise((resolvePromise, reject) => {
    // tslint:disable-next-line: no-any
    marked(markdown, (err: any, html: string) => {
      if (err) {
        return reject(err);
      }

      resolvePromise(html);
    });
  });
}

/**
 * A map of templates by name
 */
export interface TemplateMap {
  [name: string]: string;
}

/**
 * Concurrency limit
 */
export const CONCURRENCY = 2;

/**
 * Get map of partials
 *
 * @param pathToPartials Path to partials
 */
export function getPartials(pathToPartials: PathLike): TemplateMap {
  const templates: TemplateMap = {};

  for (const partial of readdirSync(pathToPartials)) {
    if (partial.match(/\.html.mustache/) === null) {
      continue;
    }

    const partialName = partial.split('.')[0];
    const buffer = readFileSync(join(pathToPartials.toString(), partial));
    templates[partialName] = buffer.toString();
  }

  return templates;
}

/**
 * Generate page for group
 *
 * @param gitlabApi GitLab API to make requests with
 * @param group Group to generate page for
 */
export async function generatePage(gitlabApi: Api, group: Group) {
  const projects: Array<Project & {
    /**
     * Has builds
     */
    $builds?: boolean;
    /**
     * Link to documentation
     */
    $documentation?: string;
  }> = (await gitlabApi.getProjectsForGroup(group.id))
    .filter((project) => {
      return project.path !== 'openstapps.gitlab.io';
    });

  projects.sort((a, b) => {
    return a.name.localeCompare(b.name);
  });

  await asyncPool(CONCURRENCY, projects, async (project) => {
    try {
      await got(`https://openstapps.gitlab.io/${project.path}`);

      project.$documentation = `https://openstapps.gitlab.io/${project.path}`;
    } catch (error) {
      Logger.warn(`Documentation for '${project.path}' does not exist.`);
    }

    const fileList = await gitlabApi.getFileList(project.id);

    project.$builds = fileList.some((file) => {
      return file.name === '.gitlab-ci.yml';
    });
  });

  const subGroups: Array<Group & {
    /**
     * Link to sub group
     */
    $link: string;
  }> = (await gitlabApi.getSubGroupsForGroup(group.id))
    .map((subGroup) => {
      return {
        ...subGroup,
        $link: `${subGroup.full_path
          .replace(/^openstapps/, 'index')
          .replace(/\//g, '_')}.html`,
      };
    });

  const pathToTemplates = resolve(__dirname, '..', 'templates');

  const templates = getPartials(join(pathToTemplates, 'partials'));

  const buffer = await readFilePromisified(join(pathToTemplates, 'index.html.mustache'));
  templates.index = buffer.toString();

  const output = mustache.render(templates.index, {
    group,
    projects,
    subGroups,
    timestamp: moment()
      .format('LLL'),
    title: group.name,
  }, templates);

  mkdirSync(resolve(__dirname, '..', 'public'), {
    recursive: true,
  });

  const outputFileName = group.full_path
    .replace(/^openstapps/, 'index')
    .replace(/\//g, '_');

  const outputFile = resolve(__dirname, '..', 'public', `${outputFileName}.html`);

  await writeFilePromisified(outputFile, output);

  Logger.ok(`Output written to '${outputFile}'.`);

  await asyncPool(CONCURRENCY, subGroups, async (subGroup) => {
    await generatePage(gitlabApi, subGroup);
  });
}

/**
 * Generate report
 *
 * @param gitlabApi GitLab API to make requests with
 * @param group Main group to generate documentation for
 */
export async function generateReport(gitlabApi: Api, group: Group) {
  const structureForTemplate = {
    group,
    issuesByAssignee: await getIssuesGroupedByAssignees((gitlabApi), 'meeting'),
    meetingDay: getNextMeetingDay(),
    timestamp: moment()
      .format('LLL'),
    title: 'Meeting report',
  };

  const reportHtml = render(
    (await readFilePromisified(resolve(__dirname, '..', 'templates', 'report.html.mustache'))).toString(),
    structureForTemplate,
    getPartials(resolve(__dirname, '..', 'templates', 'partials')),
  );

  const outputFile = resolve(__dirname, '..', 'public', `report.html`);

  await writeFilePromisified(outputFile, reportHtml);

  Logger.ok(`Output written to '${outputFile}'.`);
}

/**
 * Generate documentation
 *
 * @param gitlabApi GitLab API to make requests with
 * @param group Main group to generate documentation for
 */
export async function generateDocumentation(gitlabApi: Api, group: Group) {
  const projects = await gitlabApi.getProjectsForGroup(GROUPS[0]);

  const projectmanagement = projects.find((project) => {
    return project.path === 'projectmanagement';
  });

  if (typeof projectmanagement !== 'undefined') {
    const files: TreeFile[] = await gitlabApi
      .makeGitLabAPIRequest(`/projects/${projectmanagement.id}/repository/tree`, {
        data: {
          recursive: true,
        },
      }) as TreeFile[];

    const docFiles = files.filter((file) => {
      return file.path.indexOf('project-docs/') === 0;
    });

    const documentationTemplate = (await readFilePromisified(
      resolve(__dirname, '..', 'templates', 'documentation.html.mustache'),
    ))
      .toString();

    const navigation = docFiles
      .filter((docFile) => {
        return docFile.type === 'blob';
      })
      .sort((a, b) => {
        return a.name.localeCompare(b.name);
      })
      .map((docFile) => {
        return {
          link: generateFileName(docFile.path),
          title: basename(docFile.name, '.md'),
        };
      });

    for (const docFile of docFiles) {
      if (docFile.type !== 'blob') {
        continue;
      }

      const parts = docFile.path.split('/');
      const context = parts.slice(0, parts.length - 1);

      const content = await gitlabApi.getFile(projectmanagement.id, docFile.path, 'master');

      const outputFile = resolve(__dirname, '..', 'public', generateFileName(docFile.path));

      await writeFilePromisified(
        outputFile,
        render(
          documentationTemplate,
          {
            content: (await markedPromisified(content as string) )
              .replace(/href="([^"]+)"/g, (_, link) => {
                if (link.match(/^https?:\/\//) !== null) {
                  return `href="${link}"`;
                }

                if (link.indexOf('/') >= 0) {
                  return `href="${generateFileName(link)}"`;
                }

                return `href="${generateFileName(`${context.join('/')}/${link}`)}"`;
              })
              .replace(/<table/g, '<table class="table table-striped table-bordered table-hover"'),
            group,
            navigation,
            timestamp: moment()
              .format('LLL'),
            title: basename(docFile.path, '.md'),
          },
          getPartials(resolve(__dirname, '..', 'templates', 'partials')),
        ),
      );

      Logger.ok(`Output written to '${outputFile}'.`);
    }
  }
}

/**
 * Generate filename for path
 *
 * @param path Path to generate filename for
 */
function generateFileName(path: string) {
  return `documentation-${path.split('/')
    .join('-')
    .replace(/\.md$/, '')}.html`;
}
