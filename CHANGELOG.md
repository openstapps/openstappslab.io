## [0.5.1](https://gitlab.com/openstapps/openstapps.gitlab.io/compare/v0.5.0...v0.5.1) (2019-07-24)


### Bug Fixes

* remove link to license plates ([e5b7c49](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/e5b7c49))
* set title for overview pages ([3231840](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/3231840))



# [0.5.0](https://gitlab.com/openstapps/openstapps.gitlab.io/compare/v0.4.0...v0.5.0) (2019-07-23)


### Features

* incorporate compiled documentation ([c7f5c55](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/c7f5c55)), closes [#2](https://gitlab.com/openstapps/openstapps.gitlab.io/issues/2)



# [0.4.0](https://gitlab.com/openstapps/openstapps.gitlab.io/compare/v0.3.0...v0.4.0) (2019-05-02)



# [0.3.0](https://gitlab.com/openstapps/openstapps.gitlab.io/compare/v0.2.2...v0.3.0) (2019-04-17)


### Bug Fixes

* correct links for sub groups ([9913257](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/9913257))


### Features

* add timestamp to reports ([ee39371](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/ee39371))



## [0.2.2](https://gitlab.com/openstapps/openstapps.gitlab.io/compare/v0.2.1...v0.2.2) (2019-03-25)


### Bug Fixes

* correct template for important labels ([bbd480c](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/bbd480c))



## [0.2.1](https://gitlab.com/openstapps/openstapps.gitlab.io/compare/v0.2.0...v0.2.1) (2019-03-20)


### Features

* add link to meeting report ([44e7a61](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/44e7a61))



# [0.2.0](https://gitlab.com/openstapps/openstapps.gitlab.io/compare/v0.1.0...v0.2.0) (2019-03-20)


### Features

* add meeting report ([5f720f7](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/5f720f7))



# [0.1.0](https://gitlab.com/openstapps/openstapps.gitlab.io/compare/29d7a9e...v0.1.0) (2019-02-13)


### Features

* add support for sub groups ([b6a2165](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/b6a2165))
* make landing page prettier ([29d7a9e](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/29d7a9e))
* only show badges where appropriate ([7cc7147](https://gitlab.com/openstapps/openstapps.gitlab.io/commit/7cc7147))



