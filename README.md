# @openstapps/openstapps.gitlab.io

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/openstapps.gitlab.io.svg?style=flat-square)](https://gitlab.com/openstapps/openstapps.gitlab.io/commits/master) 
[![npm](https://img.shields.io/npm/v/@openstapps/openstapps.gitlab.io.svg?style=flat-square)](https://npmjs.com/package/@openstapps/openstapps.gitlab.io)
[![license)](https://img.shields.io/npm/l/@openstapps/openstapps.gitlab.io.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://openstapps.gitlab.io/openstapps.gitlab.io)

This project generates the main page for the `@openstapps` group.
